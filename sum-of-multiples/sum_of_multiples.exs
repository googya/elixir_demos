defmodule SumOfMultiples do
  @doc """
  Adds up all numbers from 1 to a given end number that are multiples of the factors provided.
  """
  @spec to(non_neg_integer, [non_neg_integer]) :: non_neg_integer
  def to(limit, factors) do
    to_list(limit - 1, factors, [])
    |> Enum.reduce(0, &(&1 + &2))
  end
  
  def to_list(0, _, list) do
    list
  end
  
  def to_list(n, factors, list) do
    cond do
      Enum.any?(factors, fn(t) -> rem(n, t) == 0 end) -> to_list(n - 1, factors, [n|list])
      true -> to_list(n - 1, factors, list)
    end
  end

end
