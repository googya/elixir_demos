defmodule Words do
  @doc """
  Count the number of words in the sentence.

  Words are compared case-insensitively.
  """
  @spec count(String.t) :: map
  def count(sentence) do
    sentence
    |> replace_special_character
    |> split_sentence
    |> downcase
    |> reject_nil_string
    |> count_into_map
  end
  
  defp replace_special_character(sentence) do
    String.replace(sentence, ~r/!*\&@\$%\^&/, "", global: true)
  end
  
  defp split_sentence(sentence) do
    String.split(sentence, ~r/\s+|_|:|,/)
  end
  
  defp downcase(list) do
    Enum.map(list, &String.downcase(&1))
  end
  
  defp reject_nil_string(list) do
    Enum.reject(list, fn(t) -> String.length(t) == 0 end)
  end
  
  defp count_into_map(list) do
    Enum.reduce(list, %{}, fn(x, acc) -> Map.put(acc, x, Map.get(acc, x, 0) + 1) end)
  end
  
end
