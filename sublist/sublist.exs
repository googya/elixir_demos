defmodule Sublist do
  @doc """
  Returns whether the first list is a sublist or a superlist of the second list
  and if not whether it is equal or unequal to the second list.
  """
  
  def compare([], []) do
    :equal
  end
  
  def compare([_], []) do
    :superlist
  end
  
  def compare([], [_]) do
    :sublist
  end
  
  def compare(a, a) do
    :equal
  end
  
  def compare(a, b) do
    s1 = Enum.join(a)
    s2 = Enum.join(b)
    cond do
      !Enum.all?(b, &is_integer(&1)) -> :unequal
      !Enum.all?(a, &is_integer(&1)) -> :unequal
      String.contains?(s1, s2) -> :superlist
      String.contains?(s2, s1) -> :sublist
      true -> :unequal
    end
  end
end
