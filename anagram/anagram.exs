defmodule Anagram do
  @doc """
  Returns all candidates that are anagrams of, but not equal to, 'base'.
  """
  @spec match(String.t, [String.t]) :: [String.t]
  def match(base, candidates) do
    base_list = to_sorted_charlist(base)
    candidates
    |> Enum.filter(fn(t) -> base_list == to_sorted_charlist(t) end)
    |> Enum.reject(fn(t) -> String.downcase(t) == String.downcase(base) end)
  end
  
  def to_sorted_charlist(string) do
    string |> String.downcase |> String.to_charlist |> Enum.sort
  end
end
