require IEx
defmodule RunLengthEncoder do
  @doc """
  Generates a string where consecutive elements are represented as a data value and count.
  "HORSE" => "1H1O1R1S1E"
  For this example, assume all input are strings, that are all uppercase letters.
  It should also be able to reconstruct the data into its original form.
  "1H1O1R1S1E" => "HORSE"
  """
  @spec encode(String.t) :: String.t
  def encode(string) do
    string
    |> String.split(~r/(?<=(.))(?!\1)/)
    |> Enum.reject(fn(t) -> String.length(t) ==  0 end)
    |> count_with_character
    |> Enum.join
  end
  
  defp count_with_character(list) do
    Enum.map(list, fn(t) ->
      size = Enum.count(String.to_charlist((t)))
      char = String.first(t)
      "#{size}#{char}"
    end)
  end

  @spec decode(String.t) :: String.t
  def decode(string) do
    string
    |> split_string
    |> Enum.chunk(2)
    |> flat_map
    |> Enum.join
  end
  
  def split_string(string) do
    Regex.split(~r/\d+|\w/, string, include_captures: true, trim: true)
  end
  
  def flat_map(list) do
    Enum.map(list, fn (t) ->
      num = String.to_integer(List.first(t))
      for i <- (1..num), into: "", do: List.last(t)
    end)
  end
end
