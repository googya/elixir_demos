defmodule Meetup do
  @moduledoc """
  Calculate meetup dates.
  """

  @type weekday ::
      :monday | :tuesday | :wednesday
    | :thursday | :friday | :saturday | :sunday

  @type schedule :: :first | :second | :third | :fourth | :last | :teenth

  @doc """
  Calculate a meetup date.

  The schedule is in which week (1..4, last or "teenth") the meetup date should
  fall.
  """
  
  @weekdays {:monday, :tuesday, :wednesday, :thursday, :friday, :saturday, :sunday}
  @which_to_index %{first: 0, second: 1, third: 2, fourth: 3, last: -1}

  @spec meetup(pos_integer, pos_integer, weekday, schedule) :: :calendar.date
  def meetup(year, month, weekday, schedule) do
    weekday_by_groups = weekday_by_group({year, month, 1}, %{})
    index = Map.get(@which_to_index, schedule)
    d_list = Map.get(weekday_by_groups, weekday)
    
    cond do
      schedule == :teenth ->
        d = d_list
          |> Enum.find(fn(x) -> x > 12 && x < 20 end)
        {year, month, d}
      true ->
        d = d_list |> Enum.at(index)
        {year, month, d}
    end
  end
  
  def weekday_by_group(date, day_map) do
    case :calendar.valid_date(date) do
      true ->
        {year, month, day} = date
        w = :calendar.day_of_the_week(date)
        d_name = elem(@weekdays, w - 1)
        vals = Map.get(day_map, d_name, [])
        new_vals = List.insert_at(vals, -1, day)
        new_day_map = Map.put(day_map, d_name, new_vals)
        weekday_by_group({year, month, day + 1}, new_day_map)

      false -> day_map
    end
  end
end
