defmodule Bob do
  def hey(input) do
    is_number = Regex.match?(~r/(\d+,?\s)*\d$/, input)
    cond do
      String.ends_with?(input, "?") -> "Sure."
      String.trim(input) == "" -> "Fine. Be that way!"
      String.upcase(input) == input && !is_number -> "Whoa, chill out!"
      true -> "Whatever."
    end
  end
end
