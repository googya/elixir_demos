defmodule Phone do
  @doc """
  Remove formatting from a phone number.

  Returns "0000000000" if phone number is not valid
  (10 digits or "1" followed by 10 digits)

  ## Examples

  iex> Phone.number("123-456-7890")
  "1234567890"

  iex> Phone.number("+1 (303) 555-1212")
  "3035551212"

  iex> Phone.number("867.5309")
  "0000000000"
  """
  @spec number(String.t) :: String.t
  def number(raw) do
    filted = raw
    |> String.split([".", "(", ")", "-", " ", "+"], trim: true) |> Enum.join
    str_length = String.length(filted)
    cond do
      str_length <= 9 || str_length > 12 -> "0000000000"
      str_length == 10 -> filted
      str_length == 11 ->
        if String.starts_with?(filted, "1") do
          {"1", rest } = String.split_at(filted, 1)
          rest
        else
          "0000000000"
        end
    end
  end
  
  

  @doc """
  Extract the area code from a phone number

  Returns the first three digits from a phone number,
  ignoring long distance indicator

  ## Examples

  iex> Phone.area_code("123-456-7890")
  "123"

  iex> Phone.area_code("+1 (303) 555-1212")
  "303"

  iex> Phone.area_code("867.5309")
  "000"
  """
  @spec area_code(String.t) :: String.t
  def area_code(raw) do
    phone_number = number(raw)
    {area, _} = String.split_at(phone_number, 3)
    area
  end

  @doc """
  Pretty print a phone number

  Wraps the area code in parentheses and separates
  exchange and subscriber number with a dash.

  ## Examples

  iex> Phone.pretty("123-456-7890")
  "(123) 456-7890"

  iex> Phone.pretty("+1 (303) 555-1212")
  "(303) 555-1212"

  iex> Phone.pretty("867.5309")
  "(000) 000-0000"
  """
  @spec pretty(String.t) :: String.t
  def pretty(raw) do
    phone_number = number(raw)
    [[_, a, b, c]] = Regex.scan ~r/(\d{3})(\d{3})(\d{4})/, phone_number, include_captures: true
    "(#{a}) #{b}-#{c}"
  end
end
