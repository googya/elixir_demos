defmodule Isogram do
  @doc """
  Determines if a word or sentence is an isogram
  """
  @spec isogram?(String.t) :: boolean
  def isogram?(sentence) do
    sentence
    |> String.replace([" ", "-"], "")
    |> String.codepoints
    |> map_to_number
    |> Map.values
    |> Enum.all?(&(&1==1))
  end
  
  def map_to_number(list) do
    Enum.reduce(list, Map.new, fn(x, acc) ->
      t = Map.get(acc, x)
      if t do
        Map.put(acc, x, t + 1)
      else
        Map.put(acc, x, 1)
      end
    end)
  end

end
