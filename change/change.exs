defmodule Change do
  import Enum, only: [count: 1, reverse: 1]
  import List, only: [duplicate: 2]

  def generate(amount,denomination) do
    try do
      sorted_values = denomination |> Enum.sort
     res =  sorted_values |> reverse |> change(amount)
     cond do
       res == :error -> :error
       true -> {:ok, Map.merge(Enum.reduce(sorted_values, %{}, fn(x, acc) -> Map.put(acc, x, 0) end), res) }
     end
    catch
      _, _ -> :error
    end
  end

  def change([], 0) do
    %{}
  end
  
  def change([], _) do
    :error
  end

  def change([lagest_coin|rest], amount) do
    times = div amount, lagest_coin
    remain_amount = rem amount, lagest_coin
    
    case change(rest, remain_amount) do
      :error ->
        change(rest, amount)
      res ->
        Map.put(res, lagest_coin, times)
    end
  end
end

