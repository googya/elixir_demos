defmodule School do
  @moduledoc """
  Simulate students in a school.

  Each student is in a grade.
  """

  @doc """
  Add a student to a particular grade in school.
  """
  @spec add(map, String.t, integer) :: map
  def add(db, name, grade) do
    old_grades = Map.get(db, grade, [])
    ordered_names = Enum.sort([name|old_grades], fn(a, b) -> a <= b end)
    Map.put(db, grade, ordered_names)
  end

  @doc """
  Return the names of the students in a particular grade.
  """
  @spec grade(map, integer) :: [String.t]
  def grade(db, grade) do
    Map.get(db, grade, [])
  end

  @doc """
  Sorts the school by grade and name.
  """
  @spec sort(map) :: [{integer, [String.t]}]
  def sort(db) do
    db
    |> Enum.to_list()
    |> Enum.sort(fn({x, y}, {a, b}) -> x <= a end)
  end
end
