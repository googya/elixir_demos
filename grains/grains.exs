defmodule Grains do
  @doc """
  Calculate two to the power of the input minus one.
  """
  @spec square(pos_integer) :: pos_integer
  def square(number) do
    square_cache(number - 1, 1)
  end
  
  def square_cache(0, res) do
    res
  end
  
  def square_cache(n, res) do
    new_res = 2 * res
    square_cache(n - 1, new_res)
  end

  @doc """
  Adds square of each number from 1 to 64.
  """
  @spec total :: pos_integer
  def total do
    1..64
    |> Enum.reduce(fn(t, acc) -> acc + square(t) end)
  end
end
