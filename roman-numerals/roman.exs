require IEx
defmodule Roman do
  @numbers [1000,900,500,400,100,90,50,40,10,9,5,4,1]
  @roman ["M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"]

  @doc """
  Convert the number to a roman number.
  """
  @spec numerals(pos_integer) :: String.t
  def numerals(number) do
    to_roman(number, 0, "")
  end
  
  def to_roman(0, _, str) do
    str
  end
  
  def to_roman(number, index, str) do
    cond do
      number in @numbers ->
        r_index = Enum.find_index(@numbers, &(&1 == number))
        to_roman(0, r_index, str <> Enum.at(@roman, r_index))
      number > Enum.at(@numbers, index) ->
        index_number = Enum.at(@numbers, index)
        new_number = number - index_number
        if (new_number > index_number) do
          new_index = index
          new_str = str <> Enum.at(@roman, index)
          to_roman(new_number, new_index, new_str)
        else
          new_index = index + 1
          new_str = str <> Enum.at(@roman, index)
          to_roman(new_number, new_index, new_str)
        end
      true ->  to_roman(number, index + 1, str)
    end
  end
end