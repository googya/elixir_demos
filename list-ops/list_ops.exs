defmodule ListOps do
  # Please don't use any external modules (especially List) in your
  # implementation. The point of this exercise is to create these basic functions
  # yourself.
  #
  # Note that `++` is a function from an external module (Kernel, which is
  # automatically imported) and so shouldn't be used either.

  @spec count(list) :: non_neg_integer
  def count(l) do
    count_n(l, 0)
  end
  
  def count_n([], n) do
    n
  end
  
  def count_n([_|tail], n) do
    count_n(tail, n + 1)
  end

  @spec reverse(list) :: list
  def reverse(l) do
    reverse_it(l, [])
  end
  
  def reverse_it([], res) do
    res
  end
  
  def reverse_it([head|tail], res) do
    reverse_it(tail, [head|res])
  end

  @spec map(list, (any -> any)) :: list
  def map(l, f) do
    map_it(l, f, [])
  end
  
  def map_it([], f, res) do
    res |> reverse
  end
  
  def map_it([head|tail], f, res) do
    t = f.(head)
    map_it(tail, f, [t|res])
  end

  @spec filter(list, (any -> as_boolean(term))) :: list
  def filter(l, f) do
    filter_it(l, f, [])
  end
  
  def filter_it([], f, res) do
    res |> reverse
  end
  
  def filter_it([head|tail], f, res) do
    r = f.(head)
    if r do
      filter_it(tail, f, [head|res])
    else
      filter_it(tail, f, res)
    end
  end

  @type acc :: any
  @spec reduce(list, acc, ((any, acc) -> acc)) :: acc
  def reduce([], acc, f) do
    acc
  end

  def reduce([head|tail], acc, f) do
    t = f.(head, acc)
    reduce(tail, t, f)
  end

  @spec append(list, list) :: list
  def append(a, b) do
    # for i <- b, into: a, do: i
    
    list = reverse(a)
    f = fn (x, acc) -> [x | acc] end
    reduce(list, b, f)
  end

  @spec concat([[any]]) :: [any]
  def concat([]), do: []
  def concat([h | t]) do
    append(h, concat(t))
  end
end
