defmodule SpaceAge do
  @type planet :: :mercury | :venus | :earth | :mars | :jupiter
                | :saturn | :neptune | :uranus

  @doc """
  Return the number of years a person that has lived for 'seconds' seconds is
  aged on 'planet'.
  """
  @spec age_on(planet, pos_integer) :: float
  def age_on(planet, seconds) do
    stardard_year = 60 * 60 * 24 * 365.25
    case planet do
      :earth -> seconds/stardard_year
      :mercury -> seconds/(stardard_year*0.2408467)
      :venus -> seconds/(stardard_year*0.61519726)
      :mars -> seconds/(stardard_year*1.8808158)
      :jupiter -> seconds/(stardard_year*11.862615)
      :saturn -> seconds/(stardard_year*29.447498)
      :neptune -> seconds/(stardard_year*164.79132)
      :uranus -> seconds/(stardard_year*84.016846)
    end
  end
end