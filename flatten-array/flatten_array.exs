defmodule FlattenArray do
  @doc """
    Accept a list and return the list flattened without nil values.

    ## Examples

      iex> FlattenArray.flatten([1, [2], 3, nil])
      [1,2,3]

      iex> FlattenArray.flatten([nil, nil])
      []

  """

  @spec flatten(list) :: list
  # def flatten(list) do
  #   list
  #   |> List.flatten
  #   |> Enum.reject(&(&1 == nil))
  # end
  
  def flatten(list) do
    list
    |> _flatten([])
  end
  
  def _flatten([], acc), do: acc
  
  def _flatten([nil|t], acc) do
    _flatten(t, acc)
  end
  
  def _flatten([h|tail], acc) when is_list(h)  do
    _flatten(h, _flatten(tail, acc))
  end
  
  def _flatten([x|t], acc) do
    [x|_flatten(t, acc)]
  end
end
