defmodule RNATranscription do
  @doc """
  Transcribes a character list representing DNA nucleotides to RNA

  ## Examples

  iex> RNATranscription.to_rna('ACTG')
  'UGAC'
  """
  @char_map %{?G => ?C, ?C => ?G, ?T => ?A, ?A => ?U}

  @spec to_rna([char]) :: [char]
  def to_rna(dna) do
    to_rna_map(dna, [])
  end

  def to_rna_map([], list) do
    list
  end

  def to_rna_map([head|tail], list) do
    to_rna_map(tail, list ++ [@char_map[head]])
  end
end
