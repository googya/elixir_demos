defmodule Garden do
  @doc """
  Accepts a string representing the arrangement of cups on a windowsill and a
  list with names of students in the class. The student names list does not
  have to be in alphabetical order.

  It decodes that string into the various gardens for each student and returns
  that information in a map.
  """
  
  @flower_names %{ ?G => :grass, ?C => :clover, ?R => :radishes, ?V => :violets }
  @names [:alice, :bob, :charlie, :david, :eve, :fred, :ginny, :harriet, :ileana,:joseph, :kincaid, :larry]

  @spec info(String.t(), list) :: map
  def info(info_string, student_names \\ @names) do
    index  = 0..24 |> Enum.chunk(2)
    sorted_name = sort_names(student_names)
    names_index = Enum.zip(sorted_name, index)
    info_string_list = map_info_string(info_string)
    
    Enum.reduce(names_index, %{}, fn({name, idx}, acc) ->
      val = values_with_index(info_string_list, idx) |> map_to_flower
      Map.put(acc, name, val)
    end)
  end
  
  def sort_names(names) do
    names
    |> Enum.map(&(Atom.to_string(&1)))
    |> Enum.sort
    |> Enum.map(&String.to_atom(&1))
  end
  
  def map_to_flower(list) do
    list
    |> Enum.map(fn(x) -> Map.get(@flower_names, x) end)
    |> Enum.reject(&(&1 == nil))
    |> List.to_tuple
  end
  
  def values_with_index(d_list, index) do
    Enum.reduce(d_list, [], fn(x, acc) ->
      acc ++ _values_with_index(x, index)
    end)
  end
  
  def _values_with_index(list, index) do
    Enum.reduce(index, [], fn(idx, acc) ->
      acc ++ [Enum.at(list, idx)]
    end)
  end
  
  def map_info_string(str) do
    str
    |> String.split("\n")
    |> Enum.map(&String.to_charlist(&1))
  end
end
