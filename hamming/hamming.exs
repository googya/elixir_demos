defmodule Hamming do
  @doc """
  Returns number of differences between two strands of DNA, known as the Hamming Distance.

  ## Examples

  iex> Hamming.hamming_distance('AAGTCATA', 'TAGCGATC')
  {:ok, 4}
  """
  @spec hamming_distance([char], [char]) :: non_neg_integer
  def hamming_distance(strand1, strand2) do
    l1 = List.to_string(strand1) |> String.length
    l2 = List.to_string(strand2) |> String.length
    if l1 != l2 do
      {:error, "Lists must be the same length"}
    else
      t = Enum.zip(strand1, strand2)
        |> Enum.reject(fn {x, y} -> x == y end)
        |> Enum.count
      {:ok, t}
    end
  end
end
