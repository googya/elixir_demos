defmodule BracketPush do
  @doc """
  Checks that all the brackets and braces in the string are matched correctly, and nested correctly
  """
  @lefts ["{", "(", "["]
  @rights ["}", ")", "]"]
  @spec check_brackets(String.t) :: boolean
  def check_brackets(str) do
    str
    |> String.codepoints
    |> Enum.reduce([], fn(x, acc) ->
        cond do
          Enum.member?(@lefts, x) || (Enum.member?(@rights, x) && !bracket_match?(List.last(acc), x))->
            List.insert_at(acc, -1, x)
          Enum.member?(@rights, x) && bracket_match?(List.last(acc), x) ->
            List.delete_at(acc, -1)
          true -> acc
        end
      end)
    |> Enum.empty?
  end
  
  def bracket_match?("{", "}") do
    true
  end
  
  def bracket_match?("[", "]") do
    true
  end
  
  def bracket_match?("(", ")") do
    true
  end
  
  def bracket_match?(_, _) do
    false
  end
end
